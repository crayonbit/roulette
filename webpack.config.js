const path = require('path');

module.exports = {
    entry: {
        app: './src/index.js',
        three: [
            "script-loader!./src/libs/three",
            "script-loader!./src/libs/GLTFLoader",
            "script-loader!./src/libs/OrbitControls",
        ]
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.three\.js$/,
                use: [ 'script-loader' ]
            }
        ]
    }
};
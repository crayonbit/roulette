

export default class Game {

    constructor() {

        this._numbers = [0, 2, 14, 35, 23, 4, 16, 33, 21, 6, 18, 31, 19, 8, 12, 29, 25,
            10, 27, 37, 1, 13, 36, 24, 3, 15, 34, 22, 5, 17, 32, 20, 7, 11, 30, 26, 9, 28];

        this._scene = new THREE.Scene();
        this._camera = new THREE.PerspectiveCamera(50, window.innerWidth/window.innerHeight, 0.1, 5000);

        // White directional light at half intensity shining from the top.
        const directionalLight = new THREE.DirectionalLight(0xffffff, 1.4);
        this._scene.add(directionalLight);

        this._renderer = new THREE.WebGLRenderer();
        this._renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(this._renderer.domElement);

        /*
        var controls = new THREE.OrbitControls( this._camera );
        controls.maxDistance = 10;
        controls.minDistance = 0.001;
        */

        this._camera.position.z = 10;
        this._camera.position.y = 10;
        this._camera.lookAt(new THREE.Vector3(0, 0, 0));

        this._roulette = null;
        this._ball = null;

        // Instantiate a loader
        const loader = new THREE.GLTFLoader();

        // Load a glTF resource
        loader.load(
            // resource URL
            '/AmericanRoulette/dist/models/roulette.gltf',
            // called when the resource is loaded
            (gltf) => {

                this._scene.add( gltf.scene );

                this._roulette = gltf.scene.getObjectByName("roulette");
                this._ball = gltf.scene.getObjectByName("ball");

                this._ballRotationRadius = Math.pointsDistance(
                    {x: this._ball.position.x, y: this._ball.position.z},
                    {x: this._roulette.position.x, y: this._roulette.position.z}
                );

            },
            // called while loading is progressing
            (xhr) => {

                console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );

            },
            // called when loading has errors
            (error) => {

                console.log( 'An error happened', error );

            }
        );

        this._addPolyfills();
        this.animate();
    }

    _addPolyfills() {
        Math.degToRad = (deg) => {
            return deg * Math.PI / 180;
        };
        Math.radToDeg = (rad) => {
            return rad * 180 / Math.PI;
        };
        Math.pointsDistance = (p1, p2) => {
            return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
        }
    }

    animate() {

        const ballSpeed = 0.001;
        const rouletteSpeed = 0.0001;

        let ballSpeedCounter = 0;

        let currentNumber = -1;
        let acc = 0;

        const animate = () => {
            requestAnimationFrame(animate);

            if (this._roulette) {

                //rotating the ball
                ballSpeedCounter += ballSpeed;
                const px = this._roulette.position.x + this._ballRotationRadius * Math.cos(ballSpeedCounter);
                const pz = this._roulette.position.z + this._ballRotationRadius * Math.sin(ballSpeedCounter);
                this._ball.position.x = px;
                this._ball.position.z = pz;

                //rotating the roulette
                this._roulette.rotation.y += rouletteSpeed;

                //getting normalized vectors
                const bVec = this._getNormilizedVector({x: px, y: pz});
                const rVec = this._getNormilizedVector({x: Math.sin(this._roulette.rotation.y), y: Math.cos(this._roulette.rotation.y)});

                //getting angle between ball and roulette vectors
                const rouletteToBallAngle = 180 - Math.radToDeg(this.getAngle(rVec, bVec));

                acc++;
                if (acc > 20) {
                    //console.error(rouletteToBallAngle);
                    acc = 0;
                }

                const num = this._getNumberByAngle(rouletteToBallAngle);

                if (currentNumber !== num) {
                    console.error(num);
                    currentNumber = num;
                }
            }


            this._renderer.render(this._scene, this._camera);
        };

        animate();
    }

    //???????????
    _getNormalizedRadToDeg(rad) {
        let angle = Math.abs(Math.radToDeg(rad)) % 360;
        return rad > 0 ? 360 - angle : angle;
    }

    //???????????
    _getRotationVector2(rad) {
        return new THREE.Vector2(Math.cos(rad), Math.sin(rad));
    }


    _getVectorsAngle(vec1, vec2) {
        return Math.atan2(vec1.y - vec2.y, vec1.x - vec2.x);
    }

    _getVectorMagnitude(vec) {
        return Math.sqrt(Math.pow(vec.x, 2) + Math.pow(vec.y, 2));
    }

    _getNormilizedVector(vec) {
        const magnitude = this._getVectorMagnitude(vec);
        if (magnitude > 0) {
            return {x: vec.x / magnitude, y: vec.y / magnitude};
        }
    }

    _getNumberByAngle(angle) {
        const numDist = 360 / this._numbers.length;

        for (let i = 0; i < this._numbers.length; i++) {
            let numAngle = i * numDist,
                min, max;

            //testing left half
            min = numAngle - numDist / 2;
            if (min < 0) {
                min += 360;
                max = 360;
            } else {
                max = numAngle;
            }

            if (this._isNumberInRange(angle, min, max)) {
                return this._numbers[i];
            }

            //testing right half
            max = numAngle + numDist / 2;
            if (max > 360) {
                max %= 360;
                min = 0;
            } else {
                min = numAngle;
            }

            if (this._isNumberInRange(angle, min, max)) {
                return this._numbers[i];
            }
        }
    }

    _isNumberInRange(num, min, max) {
        return num >= min && num <= max;
    }

    ///////////////// MATH

    getAngle(vectorA, vectorB) {
        return Math.atan2(this.cross(vectorA, vectorB), this.dot(vectorA, vectorB));
    }

    magnitude(vec) {
        return Math.sqrt(this.dot(vec, vec));
    }

    dot(vectorA, vectorB) {
        return vectorA.x * vectorB.x + vectorA.y * vectorB.y;
    }

    cross(vectorA, vectorB) {
        return vectorA.x * vectorB.y - vectorA.y * vectorB.x
    }
}
